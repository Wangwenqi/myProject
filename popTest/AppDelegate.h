//
//  AppDelegate.h
//  popTest
//
//  Created by Guan on 16/3/7.
//  Copyright © 2016年 Guan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

