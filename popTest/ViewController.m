//
//  ViewController.m
//  popTest
//
//  Created by Guan on 16/3/7.
//  Copyright © 2016年 Guan. All rights reserved.
//

#import "ViewController.h"
#import <POP/POP.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(100, 150, 80, 40)];
    
    [self.view addSubview:button];
    
    button.backgroundColor = [UIColor redColor];
    
    [button addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)action:(UIButton *)sender{

//    NSInteger height = CGRectGetHeight(self.view.bounds);
//    NSInteger width = CGRectGetWidth(self.view.bounds);
//    
//    CGFloat centerX = arc4random() % width;
//    CGFloat centerY = arc4random() % height;
//    
//    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
//    anim.toValue = [NSValue valueWithCGPoint:CGPointMake(centerX, centerY)];
//    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    anim.duration = 0.4;
//    
//    [sender pop_addAnimation:anim forKey:@"centerAnimation"];
    
    
    POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
    
    NSInteger height = CGRectGetHeight(self.view.bounds);
    NSInteger width = CGRectGetWidth(self.view.bounds);
    
    CGFloat centerX = arc4random() % width;
    CGFloat centerY = arc4random() % height;
    
    anim.toValue = [NSValue valueWithCGPoint:CGPointMake(centerX, centerY)];
    anim.springBounciness = 16;
    anim.springSpeed = 6;
    [sender pop_addAnimation:anim forKey:@"center"];
}

@end
